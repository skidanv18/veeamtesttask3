package com.company;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static final String MD5 = "md5";
    private static final String SHA1 = "sha1";
    private static final String SHA256 = "sha256";


    public static void main(String... args) {
        try {
            String inputFilePath = args[0];
            String targetDirectoryPath = args[1];
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

            List<String[]> targetFiles = readTargetFiles(inputFilePath);

            for (String[] targetFile : targetFiles) {
                File file = new File(String.format("%s\\%s", targetDirectoryPath, targetFile[0]));

                if (!file.exists()) {
                    System.out.println(targetFile[0] + " NOT FOUND");
                    continue;
                }

                FileInputStream fis = new FileInputStream(file);
                byte[] content = fis.readAllBytes();
                byte[] hashSum = new byte[0];
                switch (targetFile[1]) {
                    case MD5 -> hashSum = md5.digest(content);
                    case SHA1 -> hashSum = sha1.digest(content);
                    case SHA256 -> hashSum = sha256.digest(content);
                }
                fis.close();

                boolean checkFileHash = byteArrayToHex(hashSum).equals(targetFile[2]);

                System.out.printf("%s %s%n", targetFile[0], (checkFileHash ? "OK" : "FAIL"));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private static List<String[]> readTargetFiles(String inputFilePath) throws IOException {
        Scanner sc = new Scanner(Paths.get(inputFilePath));
        List<String[]> targetFiles = new LinkedList<>();
        while (sc.hasNext())
            targetFiles.add(sc.nextLine().split(" "));
        return targetFiles;
    }

    private static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append((String.format("%02x", b)));
        return sb.toString();
    }
}
